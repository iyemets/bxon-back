<?php
class Core_Controller extends MX_Controller{

    //Правила валидации
    public $rules = [];

    //Ошибки валидации
    public $errors = [];

    function __construct(){
        parent::__construct();
    }

    /**
     * @author jonston
     * @param mixed $name error name
     * @param mixed $errors error value
     */
    function setErrors($name, $errors = null){
        if(is_string($name))
            $this->errors[$name] = $errors;
        elseif(is_array($name)){
            foreach($name as $key => $error){
                $this->setErrors($key, $error);
            }
        }
    }

}