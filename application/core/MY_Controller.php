<?php
class MY_Controller extends MX_Controller{

    //Правила валидации
    protected $rules = [];

    //Ошибки валидации
    protected $errors = [];

    function __construct(){
        parent::__construct();
    }

    /**
     * @author jonston
     * @param mixed $name error name
     * @param mixed $errors error value
     */
    function setErrors($name, $errors = null){
        if(is_string($name))
            $this->errors[$name] = $errors;
        elseif(is_array($name)){
            foreach($name as $key => $error){
                $this->setErrors($key, $error);
            }
        }
    }

}