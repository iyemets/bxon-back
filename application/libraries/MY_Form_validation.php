<?php
class MY_Form_validation extends CI_Form_validation{

    function __construct(){
        parent::__construct();
    }

    function run($module = '', $group = '') {
        (is_object($module)) AND $this->CI = &$module;
        return parent::run($group);
    }

    function not_empty($str){
        $this->set_message('not_empty', 'The {field} can not be empty');
        return ! empty($str);
    }

    function valid_date($date, $format = 'Y-m-d H:i:s'){
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    function user_func($str, $params){
        $params = explode(',', $params);

        $method = array_shift($params);

        $result = call_user_func_array(['Modules', 'run'], array_merge([$method, null, $str], $params));

        return (bool) $result;
    }

    function get_errors(){
        parse_str(http_build_query($this->error_array()), $result);
        return $result;
    }

    function get_rules($field, $rule = null){
        if($this->rule_exist($field, $rule)){
            unset($this->_field_data[$field]['rules'][$rule]);
        }
    }

    function remove_rules($field, $rules){
        if(is_array($rules)){
            foreach($rules as $rule){
                $this->remove_rules($field, $rule);
            }
        }elseif(is_string($rules)){
            if($this->rule_exist($field, $rules)){
                unset($this->_field_data[$field]['rules'][$rules]);
            }
        }
    }

    function rule_exist($field, $rule){
        return isset($this->_field_data[$field]) && isset($this->_field_data[$field]['rules'][$rule]);
    }

    public function set_rules($field, $label = '', $rules = array(), $errors = array()){
        if ($this->CI->input->method() !== 'post' && empty($this->validation_data)) {
            return $this;
        }

        if (is_array($field)) {
            foreach ($field as $row) {
                if ( ! isset($row['field'], $row['rules'])) {
                    continue;
                }

                $label = isset($row['label']) ? $row['label'] : $row['field'];

                $errors = (isset($row['errors']) && is_array($row['errors'])) ? $row['errors'] : array();

                $this->set_rules($row['field'], $label, $row['rules'], $errors);
            }

            return $this;
        }

        if ( ! is_string($field) OR $field === '' OR empty($rules)) {
            return $this;
        } elseif ( ! is_array($rules)) {
            if ( ! is_string($rules)) {
                return $this;
            }

            $rules = preg_split('/\|(?![^\[]*\])/', $rules);
        }

        foreach($rules as $key => $rule){
            if(is_array($rule)){
                $matches = [
                    $rule,
                    $rule[0]
                ];
            }else{
                //echo '<pre>'; print_r($rule); echo '</pre>';
                preg_match('#^([A-Za-z0-9\_\-]+)(\[([^\]]+)\])?$#', $rule, $matches);
            }

            if($matches){
                $rules[$matches[1]] = $matches[0];
                unset($rules[$key]);
            }
        }

        $label = ($label === '') ? $field : $label;

        $indexes = array();

        if (($is_array = (bool) preg_match_all('/\[(.*?)\]/', $field, $matches)) === TRUE) {
            sscanf($field, '%[^[][', $indexes[0]);

            for ($i = 0, $c = count($matches[0]); $i < $c; $i++) {
                if ($matches[1][$i] !== '') {
                    $indexes[] = $matches[1][$i];
                }
            }
        }

        $this->_field_data[$field] = array(
            'field'		=> $field,
            'label'		=> $label,
            'rules'		=> $rules,
            'errors'	=> $errors,
            'is_array'	=> $is_array,
            'keys'		=> $indexes,
            'postdata'	=> NULL,
            'error'		=> ''
        );

        return $this;
    }

    function getFieldData(){
        return $this->_field_data;
    }

    function exist($id, $model){
        $this->set_message('exist', ucfirst($model) . ' with id "' . $id . '" is not exist');
        return (bool) $model::where('id', $id)->count();
    }
}