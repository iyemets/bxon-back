<?php
class Migration_Create_User extends CI_Migration{

    function up(){
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'label' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
            ),
            'first_name' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
            ),
            'middle_name' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
            ),
            'last_name' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
            ),
            'role' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
            ),
            'email' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
            ),
            'division_id' => array(
                'type' => 'INT',
                'constraint' => 11,
            ),
            'password' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
            ),
            'created_at' => array(
                'type' => 'DATETIME',
            ),
            'updated_at' => array(
                'type' => 'DATETIME',
            )
        ));
        $this->dbforge->add_key('id', true);
        $this->dbforge->drop_table('user', true);
        $this->dbforge->create_table('user', true);

        $data = [
            [
                'label' => 'Кузьма Петрович',
                'first_name' => 'Admin',
                'middle_name' => 'Adminovich',
                'last_name' => 'Adminov',
                'role' => 'admin',
                'password' => md5('qwerty'),
                'email' => 'admin@gmail.com'
            ],
            [
                'label' => 'Василий Михайловна',
                'first_name' => 'Pm',
                'middle_name' => 'Pmovich',
                'last_name' => 'Pmov',
                'role' => 'pm',
                'password' => md5('qwerty'),
                'email' => 'pm@gmail.com'
            ],
            [
                'label' => 'Гриша Абрамович',
                'first_name' => 'Developer',
                'middle_name' => 'Developerovich',
                'last_name' => 'Developerov',
                'role' => 'developer',
                'password' => md5('qwerty'),
                'email' => 'admin@gmail.com'
            ],
        ];

        foreach($data as $item){
            $user = new User();
            $user->fill($item);
            $user->save();
        }
    }

    function down(){
        $this->dbforge->drop_table('user', true);
    }
}