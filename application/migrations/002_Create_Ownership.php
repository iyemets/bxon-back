<?php
class Migration_Create_Ownership extends CI_Migration{

    function up(){
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => true
            ),
            'user_id' => [
                'type' => 'INT',
                'constraint' => 11
            ],
            'type' => [
                'type' => 'VARCHAR',
                'constraint' => 255
            ],
            'created_at' => [
                'type' => 'DATETIME',
            ],
            'updated_at' => [
                'type' => 'DATETIME',
            ]
        ));
        $this->dbforge->add_key('id', true);
        $this->dbforge->drop_table('ownership', true);
        $this->dbforge->create_table('ownership', true);
    }

    function down(){
        $this->dbforge->drop_table('ownership', true);
    }
}