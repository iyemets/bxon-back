<?php
class Migration_Create_Ownership_Person extends CI_Migration{

    function up(){
        $this->dbforge->add_field(array(
            'id' => [
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => true
            ],
            'ownership_id' => [
                'type' => 'VARCHAR',
                'constraint' => 128
            ],
            'first_name' => [
                'type' => 'VARCHAR',
                'constraint' => 255
            ],
            'middle_name' => [
                'type' => 'VARCHAR',
                'constraint' => 255
            ],
            'last_name' => [
                'type' => 'VARCHAR',
                'constraint' => 255
            ],
            'address' => [
                'type' => 'VARCHAR',
                'constraint' => 255
            ],
            'INN' => [
                'type' => 'VARCHAR',
                'constraint' => 255
            ],
            'site' => [
                'type' => 'VARCHAR',
                'constraint' => 255
            ],
            'email' => [
                'type' => 'VARCHAR',
                'constraint' => 255
            ],
            'phone' => [
                'type' => 'VARCHAR',
                'constraint' => 255
            ]
        ));
        $this->dbforge->add_key('id', true);
        $this->dbforge->drop_table('ownership_person', true);
        $this->dbforge->create_table('ownership_person', true);
    }

    function down(){
        $this->dbforge->drop_table('ownership_person', true);
    }
}