<?php
class Migration_Create_Ownership_Responsible extends CI_Migration{

    function up(){
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => true
            ),
            'ownership_id' => [
                'type' => 'INT',
                'constraint' => 11
            ],
            'first_name' => [
                'type' => 'VARCHAR',
                'constraint' => 255
            ],
            'middle_name' => [
                'type' => 'VARCHAR',
                'constraint' => 255
            ],
            'last_name' => [
                'type' => 'VARCHAR',
                'constraint' => 255
            ],
            'location' => [
                'type' => 'VARCHAR',
                'constraint' => 255
            ],
            'address' => [
                'type' => 'VARCHAR',
                'constraint' => 255
            ],
            'INN' => [
                'type' => 'VARCHAR',
                'constraint' => 255
            ],
            'site' => [
                'type' => 'VARCHAR',
                'constraint' => 255
            ],
            'created_at' => [
                'type' => 'DATETIME',
            ],
            'updated_at' => [
                'type' => 'DATETIME',
            ]
        ));
        $this->dbforge->add_key('id', true);
        $this->dbforge->drop_table('ownership_responsible', true);
        $this->dbforge->create_table('ownership_responsible', true);
    }

    function down(){
        $this->dbforge->drop_table('ownership_responsible', true);
    }
}