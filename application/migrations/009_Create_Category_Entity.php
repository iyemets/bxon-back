<?php
class Migration_Create_Category_Entity extends CI_Migration{

    function up(){
        $this->dbforge->add_field(array(
            'category_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'entity_id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'comment'    => 'ID сущности',
            ),
            'entity_type' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
                'comment'    => 'Тип сущности',
            )
        ));
        $this->dbforge->add_key('id', true);
        $this->dbforge->drop_table('category_entity', true);
        $this->dbforge->create_table('category_entity', true);

        $data = [
            [
                'category_id' => 1,
                'entity_id' => 1,
                'entity_type' => 'Task'
            ],
            [
                'category_id' => 2,
                'entity_id' => 1,
                'entity_type' => 'Project'
            ],
            [
                'category_id' => 2,
                'entity_id' => 2,
                'entity_type' => 'Project'
            ],
            [
                'category_id' => 3,
                'entity_id' => 1,
                'entity_type' => 'User'
            ],
            [
                'category_id' => 4,
                'entity_id' => 1,
                'entity_type' => 'User'
            ],
            [
                'category_id' => 5,
                'entity_id' => 2,
                'entity_type' => 'User'
            ]
        ];

        foreach($data as $item){
            $categories_entities = new Category_Entity();
            $categories_entities->fill($item);
            $categories_entities->save();
        }
    }

    function down(){
        $this->dbforge->drop_table('category_entity', true);
    }
}