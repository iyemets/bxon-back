<?php
class Migration_Create_Division extends CI_Migration{

    function up(){
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => 255
            ),
            'created_at' => array(
                'type' => 'DATETIME',
            ),
            'updated_at' => array(
                'type' => 'DATETIME',
            )
        ));
        $this->dbforge->add_key('id', true);
        //$this->dbforge->drop_table('division', true);
        $this->dbforge->create_table('division', true);

        $data = [
            [
                'name' => 'Подразделение 1'
            ],
            [
                'name' => 'Подразделение 2'
            ],
            [
                'name' => 'Подразделение 3',
            ],
        ];

        foreach($data as $item){
            $division = new Division();
            $division->fill($item);
            $division->save();
        }
    }

    function down(){
        $this->dbforge->drop_table('division', true);
    }
}