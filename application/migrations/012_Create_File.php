<?php

class Migration_Create_File extends CI_Migration
{

    function up()
    {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'user_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => 128,
                'comment' => 'имя файла с расширением',
            ),
            'size' => array(
                'type' => 'INT',
                'constraint' => 11,
                'comment' => 'размер файла',
            ),
            'ext' => array(
                'type' => 'VARCHAR',
                'constraint' => 10,
                'comment' => 'расширение',
            ),
            'hash' => array(
                'type' => 'VARCHAR',
                'constraint' => 128,
                'comment' => 'хэш содержимого',
            ),
            'path' => array(
                'type' => 'VARCHAR',
                'constraint' => 255
            ),
            'created_at' => array(
                "type" => "datetime"
            ),

            'updated_at' => array(
                "type" => "datetime"
            ),
        ));
        $this->dbforge->add_key('id', true);
        //$this->dbforge->drop_table('file', true);
        $this->dbforge->create_table('file', true);

        $data = [
            [
                'user_id' => 1,
                'name' => 'TZ_1.doc',
                'size' => 15,
                'ext' => 'doc',
                'path' => '/upload/2017/TZ_1.doc'
            ],
            [
                'user_id' => 1,
                'name' => 'TZ_2.doc',
                'size' => 15,
                'ext' => 'doc',
                'path' => '/upload/2017/TZ_2.doc'
            ],
            [
                'user_id' => 1,
                'name' => 'TZ_3.doc',
                'size' => 15,
                'ext' => 'doc',
                'path' => '/upload/2017/TZ_3.doc'
            ],
            [
                'user_id' => 1,
                'name' => 'TZ_4.doc',
                'size' => 15,
                'ext' => 'doc',
                'path' => '/upload/2017/TZ_4.doc'
            ],
            [
                'user_id' => 1,
                'name' => 'TZ_5.doc',
                'size' => 15,
                'ext' => 'doc',
                'path' => '/upload/2017/TZ_5.doc'
            ],

        ];

        foreach ($data as $item) {
            $user = new File();
            $user->fill($item);
            $user->save();
        }

    }

    function down(){
        $this->dbforge->drop_table('file', true);
    }

}
