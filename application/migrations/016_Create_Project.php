<?php

class Migration_Create_Project extends CI_Migration
{

    function up()
    {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'type' => array(
                'type'       => 'enum',
                'constraint' => ["regular","disposable"],
                'comment' => 'тип (абонентский, одноразовый)'
            ),
            'name' => [
                'type' => 'VARCHAR',
                'constraint' => 255,
            ],
            'description' => [
                'type' => 'TEXT'
            ],
            'date_start' => array(
                "type" => "datetime"
            ),
            'date_end' => array(
                "type" => "datetime"
            ),
            'time_limit' => array(
                'type' => 'INT',
                'constraint' => 11,
                'comment' => 'ограничение часов'
            ),
            'payment' => array(
                'type' => 'INT',
                'constraint' => 11,
                'comment' => 'сумма в месяц'
            ),
            'created_at' => [
                "type" => "datetime"
            ],
            'updated_at' => [
                "type" => "datetime"
            ]
        ));
        $this->dbforge->add_key('id', true);
        //$this->dbforge->drop_table('project', true);
        $this->dbforge->create_table('project', true);

        $data = [
            [
                'name' => 'проект 1',
                'type' => 'regular'
            ],
            [
                'name' => 'проект 2',
                'type' => 'disposable'
            ],
            [
                'name' => 'проект 3',
                'type' => 'regular'
            ],

        ];

        foreach ($data as $item) {
            $user = new Project();
            $user->fill($item);
            $user->save();
        }
    }

    function down(){
        $this->dbforge->drop_table('project', true);
    }

}
