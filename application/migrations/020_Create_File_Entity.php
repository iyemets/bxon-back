<?php

class Migration_Create_File_Entity extends CI_Migration
{

    function up()
    {
        $this->dbforge->add_field(array(
            'file_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'entity_id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'comment'    => 'ID сущности',
            ),
            'entity_type' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
                'comment'    => 'Тип сущности',
            )
        ));
        //$this->dbforge->drop_table('file_entity', true);
        $this->dbforge->create_table('file_entity', true);

        $data = [
            [
                'file_id' => 1,
                'entity_id' => 1,
                'entity_type' => 'task'
            ],
            [
                'file_id' => 2,
                'entity_id' => 1,
                'entity_type' => 'project'
            ],
            [
                'file_id' => 2,
                'entity_id' => 2,
                'entity_type' => 'project'
            ],
            [
                'file_id' => 3,
                'entity_id' => 1,
                'entity_type' => 'task'
            ],
            [
                'file_id' => 4,
                'entity_id' => 1,
                'entity_type' => 'task'
            ],
            [
                'file_id' => 5,
                'entity_id' => 2,
                'entity_type' => 'task'
            ]
        ];

        foreach ($data as $item) {
            $file_entity = new File_Entity();
            $file_entity->fill($item);
            $file_entity->save();
        }

    }

    function down(){
        $this->dbforge->drop_table('file_entity', true);
    }

}
