<?php
class Migration_Create_Site extends CI_Migration{

    function up(){
        $this->dbforge->add_field(array(
            'url' => array(
                'type' => 'TEXT',
                'comment'    => 'Урл адрес'
            ),
            'entity_id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'comment'    => 'ID сущности',
            ),
            'entity_type' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
                'comment'    => 'Тип сущности',
            )
        ));
        //$this->dbforge->drop_table('site', true);
        $this->dbforge->create_table('site', true);

        $data = [
            [
                'url' => 'test.ru',
                'entity_id' => 1,
                'entity_type' => Site::ENTITY_TYPE_PORTFOLIO
            ],
            [
                'url' => 'test.ru',
                'entity_id' => 1,
                'entity_type' => Site::ENTITY_TYPE_RESUME
            ],
            [
                'url' => 'test.ru',
                'entity_id' => 1,
                'entity_type' => Site::ENTITY_TYPE_SECURITY
            ],
            [
                'url' => 'test.ru',
                'entity_id' => 2,
                'entity_type' => Site::ENTITY_TYPE_PORTFOLIO
            ],
            [
                'url' => 'test.ru',
                'entity_id' => 2,
                'entity_type' => Site::ENTITY_TYPE_RESUME
            ],
            [
                'url' => 'test.ru',
                'entity_id' => 2,
                'entity_type' => Site::ENTITY_TYPE_SECURITY
            ]
        ];

        foreach($data as $item){
            $categories_entities = new Site();
            $categories_entities->fill($item);
            $categories_entities->save();
        }
    }

    function down(){
        $this->dbforge->drop_table('site', true);
    }
}