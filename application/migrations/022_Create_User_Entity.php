<?php
class Migration_Create_User_Entity extends CI_Migration{

    function up(){
        $this->dbforge->add_field(array(
            'user_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'entity_id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'comment'    => 'ID сущности',
            ),
            'entity_type' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
                'comment'    => 'Тип сущности',
            )
        ));
        //$this->dbforge->drop_table('user_entity', true);
        $this->dbforge->create_table('user_entity', true);

        $data = [
            [
                'user_id' => 1,
                'entity_id' => 1,
                'entity_type' => 'Task'
            ],
            [
                'user_id' => 1,
                'entity_id' => 2,
                'entity_type' => 'Task'
            ],
            [
                'user_id' => 2,
                'entity_id' => 2,
                'entity_type' => 'Task'
            ],
            [
                'user_id' => 1,
                'entity_id' => 1,
                'entity_type' => 'Project'
            ],
            [
                'user_id' => 1,
                'entity_id' => 2,
                'entity_type' => 'Project'
            ],
            [
                'user_id' => 2,
                'entity_id' => 1,
                'entity_type' => 'Project'
            ]
        ];

        foreach($data as $item){
            $categories_entities = new User_Entity();
            $categories_entities->fill($item);
            $categories_entities->save();
        }
    }

    function down(){
        $this->dbforge->drop_table('user_entity', true);
    }
}