<?php

class Migration_Create_Tag extends CI_Migration {

    function up(){
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'rank' => array(
                'type' => 'VARCHAR',
                'constraint' => 11
            ),
            'type' => array(
                'type' => 'VARCHAR',
                'constraint' => 11
            ),
            'description' => array(
                'type'       => 'VARCHAR',
                'constraint' => 128
            ),
            'created_at' => array(
                "type" => "datetime"
            ),
            'updated_at' => array(
                "type" => "datetime"
            )
        ));
        $this->dbforge->add_key('id', true);
        //$this->dbforge->drop_table('tag', true);
        $this->dbforge->create_table('tag', true);

        $data = [
            [
                'type' => 'char',
                'rank' => 'negative',
                'description' => 'Подозрение на шизофрению'
            ],
            [
                'type' => 'char',
                'rank' => 'neutral',
                'description' => 'Молчаливый'
            ],
            [
                'type' => 'char',
                'rank' => 'positive',
                'description' => 'Трудолюбивый'
            ],
            [
                'type' => 'spec',
                'rank' => 'none',
                'description' => 'Подозрение на шизофрению'
            ]
        ];

        foreach ($data as $item) {
            Tag::create($item);
        }
    }

    function down(){
        $this->dbforge->drop_table('tag', true);
    }

}
