<?php

class Migration_Create_Wallet extends CI_Migration {

    function up(){
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'user_id' => array(
                'type' => 'INT',
                'constraint' => 11
            ),
            'type' => array(
                'type' => 'VARCHAR',
                'constraint' => 128
            ),
            'value' => array(
                'type' => 'VARCHAR',
                'constraint' => 128
            ),
            'created_at' => array(
                "type" => "datetime"
            ),
            'updated_at' => array(
                "type" => "datetime"
            )
        ));
        $this->dbforge->add_key('id', true);
        //$this->dbforge->drop_table('wallet', true);
        $this->dbforge->create_table('wallet', true);

    }

    function down(){
        $this->dbforge->drop_table('wallet', true);
    }

}
