<?php
use Illuminate\Database\Eloquent\Model as Eloquent;

class Category_Entity extends Eloquent {

    protected $table = 'category_entity';

    const ENTITY_TYPE_NONE = 'none';
    const ENTITY_TYPE_PROJECT = 'project';
    const ENTITY_TYPE_TASK = 'task';
    const ENTITY_TYPE_BUG = 'bug';
    const ENTITY_TYPE_TRAINING = 'training';
    const ENTITY_TYPE_DEVELOPER = 'developer';
    const ENTITY_TYPE_USER = 'user';

    const ENTITY_TYPES = [
        'ENTITY_TYPE_NONE' => self::ENTITY_TYPE_NONE,
        'ENTITY_TYPE_PROJECT' => self::ENTITY_TYPE_PROJECT,
        'ENTITY_TYPE_TASK' => self::ENTITY_TYPE_TASK,
        'ENTITY_TYPE_BUG' => self::ENTITY_TYPE_BUG,
        'ENTITY_TYPE_TRAINING' => self::ENTITY_TYPE_TRAINING,
        'ENTITY_TYPE_DEVELOPER' => self::ENTITY_TYPE_DEVELOPER,
        'ENTITY_TYPE_USER' => self::ENTITY_TYPE_USER
    ];

    protected $fillable = [
        'category_id',
        'entity_type',
        'entity_id',
    ];

    public $primaryKey = 'category_id';
    public $timestamps = false;
}
