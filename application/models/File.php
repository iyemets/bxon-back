<?php

use Illuminate\Database\Eloquent\Model as Eloquent;

class File extends Eloquent {

    protected $table = 'file';

    public $primaryKey = 'id';
    public $timestamps = true;
    public $hidden = array('pivot');
    public $fillable = [
        'user_id',
        'name',
        'size',
        'ext',
        'hash',
        'path',
        'is_private',
        'created_at'
    ];

    function file_entity(){
        return $this->hasMany('File_entity', 'file_id');
    }

    function resumes(){
        return $this->morphedByMany('File', 'entity', 'file_entity');
    }

    function portfolio(){
        return $this->morphedByMany('File', 'entity', 'file_entity');
    }


}
