<?php
use Illuminate\Database\Eloquent\Model as Eloquent;

class Ownership extends Eloquent {

    protected $table = 'ownership';

    protected $fillable = [
        'type'
    ];

    const TYPE_SP = 'sp';
    const TYPE_PERSON = 'person';
    const TYPE_ORGANIZATION = 'organization';

    const TYPES = [
        self::TYPE_ORGANIZATION,
        self::TYPE_PERSON,
        self::TYPE_SP
    ];

    function sp(){
        return $this->hasOne('Ownership_Sp');
    }

    function person(){
        return $this->hasOne('Ownership_Person');
    }

    function organization(){
        return $this->hasOne('Ownership_Organization');
    }

    function responsible(){
        return $this->hasOne('Ownership_Responsible');
    }

    function files(){
        return $this->morphToMany('File', 'entity', 'file_entity');
    }

    function user(){
        return $this->belongsTo('User', 'user_id', 'id');
    }

    function delete(){
        $this->files()->delete();
        $this->responsible()->delete();
        $this->organization()->delete();
        $this->sp()->delete();
        $this->person()->delete();

        parent::delete();
    }

}
