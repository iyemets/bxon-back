<?php
use Illuminate\Database\Eloquent\Model as Eloquent;

class Ownership_Responsible extends Eloquent {

    protected $table = 'ownership_responsible';

    public $timestamps = false;

    protected $fillable = [
        'ownership_id',
        'first_name',
        'middle_name',
        'last_name',
        'location',
        'address',
        'INN',
        'site',
        'contacts'
    ];

}
