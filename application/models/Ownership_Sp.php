<?php
use Illuminate\Database\Eloquent\Model as Eloquent;

class Ownership_Sp extends Eloquent {

    protected $table = 'ownership_sp';

    public $timestamps = false;

    protected $fillable = [
        'ownership_id',
        'full_name',
        'OGRN',
        'number',
        'date',
        'postal_address',
        'physical_address',
        'OKATO',
        'OPF',
        'OKVED',
        'phone',
        'email',
        'site',
        'contacts'
    ];

}
