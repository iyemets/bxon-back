<?php

use Illuminate\Database\Eloquent\Model as Eloquent;

class Portfolio extends Eloquent {

    protected $table = 'portfolio';

    public $timestamps = true;
    public $primaryKey = 'id';
    protected $fillable = [
        'description'
    ];

    function sites(){
        return $this->morphMany('Site', 'entity');
    }

    function files(){
        return $this->morphToMany('File', 'entity', 'file_entity');
    }

    function user(){
        return $this->belongsTo('User');
    }

    function delete(){
        $this->files->delete();
        $this->sites->delete();
        parent::delete();
    }

}