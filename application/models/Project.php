<?php

use Illuminate\Database\Eloquent\Model as Eloquent;

class Project extends Eloquent{

    const TYPE_DISPOSABLE = 'disposable'; // одноразовый
    const TYPE_REGULAR = 'regular'; // абонентский

    const ENTITY_PROJECT = 'project';

    protected $table = 'project';

    public $timestamps = true;

    protected $fillable = [
        'name',
        'type',
        'description',
        'date_start',
        'date_end',
        'time_limit',
        'payment'
    ];

    function categories(){
        return $this->morphToMany('Category', 'entity', 'category_entity');
    }

    public function access() {
        return $this->hasMany('Project_access', 'project_id');
    }

    function tasks(){
        return $this->hasMany('Task');
    }

    public function files() {
        return $this->morphToMany('File', 'entity', 'file_entity');
    }

    public function users() {
        return $this->morphToMany('User', 'entity', 'user_entity');
    }

}