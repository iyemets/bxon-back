<?php
use Illuminate\Database\Eloquent\Model as Eloquent;

class Project_Access extends Eloquent {

    const PROTOCOL_FTP = 'ftp';
    const PROTOCOL_SSH = 'ssh';
    const PROTOCOL_ADMIN_PANEL = 'admin_panel';
    const PROTOCOL_MISC = 'misc';

    const INSPECTION_AUTO = 'auto';
    const INSPECTION_MANUAL = 'manual';

    const ENTITY_PROJECT_ACCESS = 'project_access';

    //public $hidden = array('project_id');
    public $guarded = array();
    public $timestamps = true;

    protected $table = 'project_access';

    function project(){
        return $this->hasOne('Project', 'id', 'project_id');
    }

    function group(){
        return $this->hasOne('Project_Access_Group', 'id', 'access_group_id');
    }

    public function files() {
        return $this->morphToMany('File', 'entity', 'file_entity');
    }

}
