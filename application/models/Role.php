<?php

use Illuminate\Database\Eloquent\Model as Eloquent;

class Role extends Eloquent {

    const ROLE_ADMIN = 'admin';
    const ROLE_PM = 'pm';
    const ROLE_UM = 'um';
    const ROLE_CUSTOMER = 'customer';
    const ROLE_DEVELOPER = 'developer';
    const ROLE_OBSERVER = 'observer';
    const ROLE_BOT = 'bot';

    const ROLES = [
        'ROLE_ADMIN' => self::ROLE_ADMIN,
        'ROLE_PM' => self::ROLE_PM,
        'ROLE_UM' => self::ROLE_UM,
        'ROLE_CUSTOMER' => self::ROLE_CUSTOMER,
        'ROLE_DEVELOPER' => self::ROLE_DEVELOPER,
        'ROLE_OBSERVER' => self::ROLE_OBSERVER,
        'ROLE_BOT' => self::ROLE_BOT
    ];

    public $timestamps = false;

    protected $table = 'role';

}