<?php

use Illuminate\Database\Eloquent\Model as Eloquent;

class Site extends Eloquent {

    protected $table = 'site';

    const ENTITY_TYPE_NONE = 'none';
    const ENTITY_TYPE_SECURITY = 'security';
    const ENTITY_TYPE_RESUME = 'resume';
    const ENTITY_TYPE_PORTFOLIO = 'portfolio';

    const ENTITY_TYPES = [
        'ENTITY_TYPE_NONE' => self::ENTITY_TYPE_NONE,
        'ENTITY_TYPE_SECURITY' => self::ENTITY_TYPE_SECURITY,
        'ENTITY_TYPE_RESUME' => self::ENTITY_TYPE_RESUME,
        'ENTITY_TYPE_PORTFOLIO' => self::ENTITY_TYPE_PORTFOLIO
    ];

    public $timestamps = false;
    public $primaryKey = 'id';
    protected $fillable = ['url', 'user_id', 'entity_id', 'entity_type'];

    function user(){
        return $this->belongsTo('user');
    }

}