<?php

use Illuminate\Database\Eloquent\Model as Eloquent;

class Task extends Eloquent{

    protected $table = 'task';

    protected $fillable = [
        'name',
        'project_id'
    ];

    function categories(){
        return $this->morphToMany('Category', 'entity', 'categories_entities');
    }

    function users(){
        return $this->morphToMany('User', 'entity', 'user_entity');
    }

    function project(){
        return $this->belongsTo('Project');
    }


}