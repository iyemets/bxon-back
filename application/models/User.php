<?php

use Illuminate\Database\Eloquent\Model as Eloquent;

class User extends Eloquent {

    protected $table = 'user';

    protected $fillable = [
        'first_name',
        'middle_name',
        'last_name',
        'email',
        'role',
        'password',
        'division_id'
    ];

    public $timestamps = true;
    public $hidden = array('password', 'pivot');

    function bank(){
        return $this->hasOne('Bank');
    }

    function ownership(){
        return $this->hasOne('Ownership');
    }

    function projects(){
        return $this->morphedByMany('Project', 'entity', 'user_entity');
    }

    function tasks(){
        return $this->morphedByMany('Task', 'entity', 'user_entity');
    }

    function tags(){
        return $this->morphedByMany('Tag', 'entity', 'user_entity');
    }

    function entities(){
        return $this->hasMany('User_Entity');
    }

    public function properties(){
        return $this->hasMany('Property', 'user_id');
    }

    public function division(){
        return $this->hasOne('Division', 'id', 'division_id');
    }

    function categories(){
        return $this->morphToMany('Category', 'entity', 'category_entity');
    }

    function resume(){
        return $this->hasOne('Resume');
    }

    function portfolio(){
        return $this->hasOne('Portfolio');
    }

    function schedule(){
        return $this->hasMany('Schedule');
    }

    function payments(){
        return $this->hasOne('Payment');
    }

    function wallets(){
        return $this->hasMany('Wallet');
    }

    function delete() {
        $this->resume()->delete();
        $this->portfolio()->delete();
        $this->payments()->delete();
        $this->wallets()->delete();
        $this->schedule()->delete();
        $this->categories()->detach();
        $this->properties()->delete();
        $this->entities()->delete();
        $this->ownership()->delete();
        parent::delete();
    }
}