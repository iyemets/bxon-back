<?php

use Illuminate\Database\Eloquent\Model as Eloquent;

class Wallet extends Eloquent {
    protected $table = 'wallet';

    public $timestamps = true;
    public $primaryKey = 'id';
    protected $fillable = ['type', 'value'];

    function user(){
        return $this->belongsTo('User');
    }
}