<?php
class Category_api extends Api_Controller{


    function __construct(){
        parent:: __construct();
    }

    function get($id = null){
        if($id){
            $this->output->send($this->_getById($id));
        }else{
            $this->output->send($this->_getById($id), 200);
        }
    }

    function _getById($id){
        $category = Category::where('id', $id);

        if( ! $category->count()){
            return null;
        }

        return $category->first()->toArray();
    }

    function _getList(){
        return Category::get()->toArray();
    }

    function add(){

    }

    function edit(){

    }

    function remove(){

    }

    function _exist($id){
        return (bool) Category::where('id', $id)->count();
    }

    function _bind($category_id, $entity_type, $entity_id){
        if( ! $this->_exist($category_id)) return false;

        if( ! in_array($entity_type, Category_Entity::ENTITY_TYPES)){
            return false;
        }

        $category_entity = new Category_Entity();
        $category_entity->category_id = $category_id;
        $category_entity->entity_type = ucfirst($entity_type);
        $category_entity->entity_id = $entity_id;

        return $category_entity->save();
    }


    function _unbind($entity_type, $entity_id){
        $remove = Category_Entity::where([
            'entity_id' => $entity_id,
            'entity_type' => $entity_type
        ])->delete();
        return $remove;
    }
}