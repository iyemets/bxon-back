<?php
class Ownership_api extends Api_Controller{

    function __construct(){
        parent:: __construct();
    }

    function _save($data, Ownership $ownership = null){

        $ownership->fill($data);
        $ownership->save();

        if($ownership->type === Ownership::TYPE_ORGANIZATION){
            $ownership_entity = Ownership_Organization::firstOrNew(['ownership_id' => $ownership->id]);
        }elseif($ownership->type === Ownership::TYPE_PERSON){
            $ownership_entity = Ownership_Person::firstOrNew(['ownership_id' => $ownership->id]);
        }elseif($ownership->type === Ownership::TYPE_SP){
            $ownership_entity = Ownership_Sp::firstOrNew(['ownership_id' => $ownership->id]);
        }

        if( ! isset($data[$ownership->type])){
            $data[$ownership->type] = [];
        }
        $ownership_entity->fill($data[$ownership->type]);
        $ownership_entity->save();

        if(isset($data['files'])){
            $ownership_files = $ownership->files();
            $ownership_files->detach();
            $ownership_files->attach($data['files']);
        }

        return ['ownership_id' => $ownership->id];
    }


}
