<?php
class Portfolio_api extends Api_Controller{

    function __construct(){
        parent:: __construct();
    }

    function _save($data, Portfolio $portfolio = null){
        if( ! $portfolio)
            $portfolio = new Portfolio();

        $portfolio->fill($data);
        $portfolio->save();

        if(isset($data['sites'])){
            $portfolio_sites = $portfolio->sites();
            $portfolio_sites->delete();
            $sites = [];
            foreach( (array) $data['sites'] as $site){
                $sites[] = new Site([
                    'url' => $site
                ]);
            }
            $portfolio_sites->saveMany($sites);
        }

        if(isset($data['files'])){
            $portfolio_files = $portfolio->files();
            $portfolio_files->detach();
            $portfolio_files->attach($data['files']);
        }
    }

    function _remove(Portfolio $portfolio){
        $portfolio->delete();
        $portfolio->sites()->delete();
        $portfolio->files()->delete();
    }
}