<?php
class Resume_api extends Api_Controller{

    function __construct(){
        parent:: __construct();
    }


    function _get($filters = null){
        $resume = new Resume();

        if(isset($filters['user_id'])){
            $resume = $resume->where('user_id', $filters['user_id']);
        }

        if(isset($filters['id'])){
            $resume = $resume->where('id', $filters['id']);
        }

        return $resume->get();
    }

    function _save($data, Resume $resume = null){
        if( ! $resume)
            $resume = new Resume();

        $resume->fill($data);
        $resume->save();

        if(isset($data['sites'])){
            $resume_sites = $resume->sites();
            $resume_sites->delete();
            $sites = [];
            foreach( (array) $data['sites'] as $site){
                $sites[] = new Site([
                    'url' => $site
                ]);
            }
            $resume_sites->saveMany($sites);
        }

        if(isset($data['files'])){
            $resume_files = $resume->files();
            $resume_files->detach();
            $resume_files->attach($data['files']);
        }
    }

    function _remove(Resume $resume){
        $resume->delete();
        $resume->sites()->delete();
        $resume->files()->delete();
    }

    function _exist($resume_id){
        return (bool) User_Resume::find($resume_id)->count();
    }

}