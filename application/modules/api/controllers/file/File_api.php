<?php
use Illuminate\Database\Capsule\Manager as DB;

class File_api extends Api_Controller
{

    function __construct()
    {
        parent::__construct();
    }

    function index()
    {
    }

    function getList(){

        $page       = intval(get('page'));
        $limit      = intval(get('limit'));
        $filters    = get('filters');
        $sort_by    = get('sort_by');
        $sort_order = get('sort_order');

        if( ! $page) $page = 1;

        $offset = $page * $limit - $limit;

        $files = $this->_getList($filters, $sort_by, $sort_order, $offset, $limit);

        $this->output->send($files, 200);
    }

    function _getList($filters = null, $sort_by = null, $sort_order = null, $offset = null, $limit = 10){

        if( ! in_array($sort_by, ['id', 'size', 'ext'])){
            $sort_by = 'id';
        }

        if( ! in_array(mb_strtolower($sort_order), ['asc', 'desc'])){
            $sort_order = 'asc';
        }

        $limit = (int) $limit;

        if($limit < 1 || $limit > 100){
            $limit = 10;
        }

        $offset = (int) $offset;

        $file = new File();

        $file = $file->with('file_entity');

        if(isset($filters['user_id'])){
            $file = $file->where(
                'user_id', '=', $filters['user_id']
            );
        }


        $total = $file->count();

        $file = $file->skip($offset)->take($limit)->orderBy($sort_by, $sort_order)->get();

        if ( ! $file->count()) return null;

        $file['pagination'] = array(
            "limit" => $limit,
            "total" => $total
        );

        return $file->toArray();
    }

    /*
    |--------------------------------------------------------------------------
    | Загрузка файла
    |--------------------------------------------------------------------------
    */

    function upload()
    {

        //TODO: указать id текущего авторизированного юзера
        $this->user_id = 1;
        
        if (!isset($_FILES['files'])) return false;

        $temp_files = $_FILES;
        $this->load->library('upload');
        $this->dir = './upload/' . date('Y');
        if (!file_exists($this->dir)) {
            mkdir($this->dir);
        }

        $data = [];

        $count = count($_FILES['files']['name']);
        for ($i = 0; $i <= $count - 1; $i++) {
            $_FILES['files'] = array(
                'name' => $temp_files['files']['name'][$i],
                'type' => $temp_files['files']['type'][$i],
                'tmp_name' => $temp_files['files']['tmp_name'][$i],
                'error' => $temp_files['files']['error'][$i],
                'size' => $temp_files['files']['size'][$i],
            );
            $hash = hash_file('crc32', $temp_files['files']['tmp_name'][$i]);
            $ext = explode(".", $temp_files['files']['name'][$i]);
            $file_name = $hash . '_' . $this->user_id . '.' . end($ext);
            $checkFile = $this->_checkFile($hash, $this->user_id, end($ext));
            if ($checkFile) {
                $data[] = array(
                    'id' => $checkFile[0]['id'],
                    'path' => '/' . $checkFile[0]['path'],
                );
            } else {
                $file_data = array(
                    'file_name' => $file_name,
                    'upload_path' => $this->dir,
                    'allowed_types' => '*',
                );
                $this->upload->initialize($file_data);
                if ($this->upload->do_upload('files')) {
                    $tmp_data = $this->upload->data();
                    $files_data[$i]['data'] = $tmp_data['full_path'];

                    $path = '/' . ltrim($this->dir, "/.") . '/' . $file_name;

                    $file = array(
                        'user_id' => $this->user_id,
                        'name' => $temp_files['files']['name'][$i],
                        'size' => $temp_files['files']['size'][$i],
                        'ext' => end($ext),
                        'hash' => $hash,
                        'path' => $path
                    );


                    $data[] = array(
                        'id' => File::create($file)->id,
                        'path' => $path
                    );
                }
                else $this->output->send(['errors' => ['upload' => 'errors.upload']], 500);
            }
        }
        $this->output->send($data, 200);
    }


    /*
    |--------------------------------------------------------------------------
    | Удаление файов
    |--------------------------------------------------------------------------
    */

    function _remove($file_ids)
    {

        if (!is_array($file_ids)) {
            return false;
        }

        $files = File::whereIn("id", $file_ids)->get();

        if (!$files->count()) {
            return false;
        }

        foreach ($files as $file) {

            $file_path = $_SERVER['DOCUMENT_ROOT'] . $file['path'];
            if (file_exists($file_path)) {
                unlink($file_path);
                File::where('id', $file['id'])->delete();
                File_Entity::where(['file_id' => $file['id']])->delete();
            }
            else{
                unlink($file_ids[$file['id']]);
            }

        }

        return $file_ids;
    }

    /*
    |--------------------------------------------------------------------------
    | Получить файл по id
    |--------------------------------------------------------------------------
    */

    function get($id)
    {
        $file = File::where('id', $id);

        if( ! $file->count()){
            $this->output->send(null, 200);
        }

        $file = $file->first()->toArray();

        $this->output->send($file, 200);
    }

    /*
    |--------------------------------------------------------------------------
    | Проверка файла на существование в таблице
    |--------------------------------------------------------------------------
    */

    public function _checkFile($hash, $user_id, $ext)
    {
        $files = File::where([
            "hash" => $hash,
            "user_id" => $user_id,
            "ext" => $ext
        ])->get();


        if (!$files->isEmpty()) {
            return $files->toArray();
        } else {
            return false;
        }
    }

    /*
    |--------------------------------------------------------------------------
    | Проверяем файл на существование в БД
    |--------------------------------------------------------------------------
    */

    function _fileExist($id){
        return (bool) File::where('id', $id)->count();
    }

}