<?php
use Illuminate\Database\Capsule\Manager as DB;
class Project_Access_api extends Api_Controller
{

    protected $protocols = [
        Project_Access::PROTOCOL_FTP,
        Project_Access::PROTOCOL_SSH,
        Project_Access::PROTOCOL_ADMIN_PANEL,
        Project_Access::PROTOCOL_MISC,
    ];
    protected $inspections = [
        Project_Access::INSPECTION_AUTO,
        Project_Access::INSPECTION_MANUAL
    ];

    function __construct()
    {
        parent:: __construct();
        $this->load->module('api/file/File_Entity_api', null, 'file_entity');
    }

    function index()
    {

    }

    /*
    |--------------------------------------------------------------------------
    | Получить список доступов к проекту
    |--------------------------------------------------------------------------
    */

    function getList(){
        $page = intval(get('page'));
        $limit = intval(get('limit'));
        $filters = get('filters');
        $sort_by = get('sort_by');
        $sort_order = get('sort_order');

        if (!$page) $page = 1;

        $offset = $page * $limit - $limit;

        $accesses = $this->_getList($filters, $sort_by, $sort_order, $offset, $limit);

        $this->output->send($accesses, 200);
    }

    function _getList($filters = null, $sort_by = null, $sort_order = null, $offset = null, $limit = 10)
    {

        if (!in_array($sort_by, ['id', 'project_id', 'protocol', 'access_group_id'])) {
            $sort_by = 'id';
        }

        if (!in_array(mb_strtolower($sort_order), ['asc', 'desc'])) {
            $sort_order = 'asc';
        }

        $limit = (int)$limit;

        if ($limit < 1 || $limit > 100) {
            $limit = 10;
        }

        $offset = (int)$offset;

        $accesses = new Project_Access();

        $accesses = $accesses->with('files', 'group');

        if (isset($filters['project_id'])){
            $accesses = $accesses->where('project_id', (int)$filters['project_id']);
        }

        if (isset($filters['access_group_id'])){
            $accesses = $accesses->where('access_group_id', (int)$filters['access_group_id']);
        }

        if (isset($filters['protocol']) && in_array($filters['protocol'], $this->protocols)){
            $accesses = $accesses->where('protocol', $filters['protocol']);
        }

        $total = $accesses->count();
        $accesses = $accesses->skip($offset)->take($limit)->orderBy($sort_by, $sort_order)->get();

        $count = $accesses->count();

        if (!$count) return null;

        $accesses['pagination'] = array(
            "limit" => $limit,
            "total" => $total
        );

        return $accesses->toArray();
    }

    /*
    |--------------------------------------------------------------------------
    | Установка валидации
    |--------------------------------------------------------------------------
    */

    function _setValidation($data)
    {
        $validation = $this->form_validation;
        $validation->set_data($data);

        $validation->set_rules('project_id', 'project_id', 'required|integer');
        $validation->set_rules('access_group_id', 'access_group_id', 'required|integer');
        $validation->set_rules('protocol', 'protocol', 'in_list[' . implode(',', $this->protocols) . ']');
        $validation->set_rules('title', 'title', 'required|max_length[256]');
        $validation->set_rules('inspection', 'inspection', 'required|in_list[' . implode(',', $this->inspections) . ']');
        $validation->set_rules('server', 'server', 'max_length[100]');
        $validation->set_rules('login', 'login', 'max_length[100]');
        $validation->set_rules('password', 'password', 'max_length[100]');
        $validation->set_rules('comment', 'comment', 'max_length[300]');

        if (isset($data['project_id'])) {
            $validation->set_rules('project_id', 'project_id', 'trim|required|numeric|user_func[api/project/Project_api/_projectExist]', ['user_func' => 'Project not exist']);
        }

        if (isset($data['access_group_id'])) {
            $validation->set_rules('access_group_id', 'access_group_id', 'trim|required|numeric|user_func[api/project/Project_Access_Group_api/_accessGroupExist]', ['user_func' => 'Access Group not exist']);
        }

        return $validation;
    }

    /*
    |--------------------------------------------------------------------------
    | Добавить доступ к проекту
    |--------------------------------------------------------------------------
    */

    function add()
    {
        $data = json();

        $validation = $this->_setValidation($data);

        if (!$validation->run($this)) {
            $this->output->send($validation->get_errors(), 422);
        }

        $access = new Project_Access();

        $access->fillable([
            "access_group_id",
            "project_id",
            "protocol",
            "title",
            "inspection",
            "server",
            "login",
            "password",
            "comment"
        ]);

        $access->fill($data);

        if (!$access->save()) {
            $this->output->send(['access' => 'error.project_access.update'], 500);
            return false;
        }

        $file_ids = isset($data['file_ids']) ? $data['file_ids'] : [];
        $this->file_entity->_bind(Project_Access::ENTITY_PROJECT_ACCESS, $access->id, $file_ids);

        $this->output->send(['access_id' => $access->id], 200);
    }

    /*
    |--------------------------------------------------------------------------
    | Существует ли доступ
    |--------------------------------------------------------------------------
    */

    function _accessExist($id){
        return (bool) Project_Access::where('id', $id)->count();
    }

    /*
    |--------------------------------------------------------------------------
    | Получить доступ
    |--------------------------------------------------------------------------
    */

    function get($project_id, $access_id){
        $access = Project_Access::where('id', $access_id);

        if( ! $access->count()){
            $this->output->send(null, 200);
        }

        $access = $access->with('files', 'group')->first()->toArray();

        $this->output->send($access, 200);
    }

    /*
    |--------------------------------------------------------------------------
    | Редактировать доступы
    |--------------------------------------------------------------------------
    */

    function edit($project_id, $access_id){
        if( ! $this->_accessExist($access_id)){
            $this->output->send(['errors' => 'error.project_access.not_exist']);
        }

        $data = json();

        $validation = $this->_setValidation($data);

        if (!$validation->run($this)) {
            $this->output->send($validation->get_errors(), 422);
        }

        $access = Project_Access::where('id', $access_id)->first();

        $access->fillable([
            "access_group_id",
            "project_id",
            "protocol",
            "title",
            "inspection",
            "server",
            "login",
            "password",
            "comment"
        ]);

        $access->fill($data);

        if (!$access->save()) {
            $this->output->send(['access' => 'error.project_access.update'], 500);
            return false;
        }

        $file_ids = isset($data['file_ids']) ? $data['file_ids'] : [];
        $this->file_entity->_unbind(Project_Access::ENTITY_PROJECT_ACCESS, $access->id);
        $this->file_entity->_bind(Project_Access::ENTITY_PROJECT_ACCESS, $access->id, $file_ids);

        $this->output->send(['access_id' => $access->id], 200);
    }

    /*
    |--------------------------------------------------------------------------
    | Удалить доступы
    |--------------------------------------------------------------------------
    */

    function remove($project_id, $access_id)
    {
        if ($this->_remove($project_id, $access_id)) {
            $this->output->send(['access_id' => $access_id], 200);
        } else {
            $this->output->send(['errors' => ['delete_access' => 'errors.access.delete']], 500);
        }
    }

    function _remove($project_id, $access_id)
    {
        DB::beginTransaction();
        $access = Project_Access::where('id', $access_id);

        $this->file_entity->_unbind(Project_Access::ENTITY_PROJECT_ACCESS, $access_id);

        if ($access->delete()) {
            DB::commit();
           return true;
        } else {
            DB::rollback();
            return false;
        }
    }


}
