<?php
use Illuminate\Database\Capsule\Manager as DB;

class Project_api extends Api_Controller
{

    protected $types = [
        Project::TYPE_DISPOSABLE,
        Project::TYPE_REGULAR
    ];

    function __construct()
    {
        parent:: __construct();
    }

    function index()
    {
        echo 'index';
    }

    /*
    |--------------------------------------------------------------------------
    | Получаем список проектов
    |--------------------------------------------------------------------------
    */

    function getList()
    {

        $page = intval(get('page'));
        $limit = intval(get('limit'));
        $filters = get('filters');
        $sort_by = get('sort_by');
        $sort_order = get('sort_order');

        if (!$page) $page = 1;

        $offset = $page * $limit - $limit;

        $projects = $this->_getList($filters, $sort_by, $sort_order, $offset, $limit);

        $this->output->send($projects, 200);
    }

    function _getList($filters = null, $sort_by = null, $sort_order = null, $offset = null, $limit = 10)
    {

        if (!in_array($sort_by, ['id', 'type', 'date_start', 'date_end', 'created_at'])) {
            $sort_by = 'id';
        }

        if (!in_array(mb_strtolower($sort_order), ['asc', 'desc'])) {
            $sort_order = 'asc';
        }

        $limit = (int)$limit;

        if ($limit < 1 || $limit > 100) {
            $limit = 10;
        }

        $offset = (int)$offset;

        $projects = new Project();

        $projects = $projects->with('files', 'users');

        $total = $projects->count();
        $projects = $projects->skip($offset)->take($limit)->orderBy($sort_by, $sort_order)->get();

        $count = $projects->count();

        if (!$count) return null;

        $projects['pagination'] = array(
            "limit" => $limit,
            "total" => $total
        );

        return $projects->toArray();
    }

    /*
    |--------------------------------------------------------------------------
    | Получаем проекты по id
    |--------------------------------------------------------------------------
    */

    function get($id)
    {
        $project = Project::where('id', $id)->with('files', 'users')->first();

        if ($project) {
            $this->output->send($project, 200);
        } else {
            $this->output->send(null, 200);
        }
    }

    /*
    |--------------------------------------------------------------------------
    | Установка правил валидации
    |--------------------------------------------------------------------------
    */

    function _setValidation($data)
    {

        $validation = $this->form_validation;
        $validation->set_data($data);

        $validation->set_rules([
            array(
                'field' => 'name',
                'label' => 'name',
                'rules' => 'required|max_length[64]' //is_unique[project.name] не пашет
            ),
            array(
                'field' => 'type',
                'label' => 'type',
                'rules' => 'required|in_list[' . implode(',', $this->types) . ']'
            )

        ]);

        if (isset($data['type']) && $data['type'] == Project::TYPE_DISPOSABLE) {
            $validation->set_rules([
                array(
                    'field' => 'date_start',
                    'label' => 'date_start',
                    'rules' => 'required|valid_date', // |valid_date //не пашет блеать
                    'errors' => array(//'valid_date' => 'Date is not valid %s.',
                    ),
                ),
                array(
                    'field' => 'date_end',
                    'label' => 'date_end',
                    'rules' => 'required|valid_date', // |valid_date //не пашет блеать
                    'errors' => array(//'valid_date' => 'Date is not valid %s.',
                    ),
                ),
                array(
                    'field' => 'time_limit',
                    'label' => 'time_limit',
                    'rules' => 'required|integer'
                ),
                array(
                    'field' => 'payment',
                    'label' => 'payment',
                    'rules' => 'required|integer'
                )
            ]);
        }

        return $validation;
    }

    /*
    |--------------------------------------------------------------------------
    | Создаем проект
    |--------------------------------------------------------------------------
    */

    function add()
    {

        $data = json();

        $validation = $this->_setValidation($data);

        if (!$validation->run($this)) {
            $this->output->send($validation->get_errors(), 422);
        }

        $project = new Project();
        $project->fillable([
            'name',
            'type',
            'description',
            'date_start',
            'date_end',
            'time_limit',
            'payment'
        ]);
        $project->fill($data);

        if (!$project->save()) {
            $this->output->send(['project' => 'error.add_project'], 500);
            return false;
        }


        $data['file_ids'] = json('file_ids');
        $data['user_ids'] = json('user_ids');

        //привязываем загруженные файлы к проекту
        if ($data['file_ids']) {
            $this->load->module('api/file/File_Entity_api', null, 'file_entity');
            $this->file_entity->_bind(Project::ENTITY_PROJECT, $project->id, $data['file_ids']);
        }

        //привязываем пользователей к проекту
        if ($data['user_ids']) {
            $this->load->module('api/user/User_Entity_api', null, 'user_entity');
            $this->user_entity->_bind(Project::ENTITY_PROJECT, $project->id, $data['user_ids']);
        }

        $this->output->send(['project_id' => $project->id], 200);
    }

    /*
    |--------------------------------------------------------------------------
    | Редактирование данных проекта
    |--------------------------------------------------------------------------
    */

    function edit($project_id)
    {

        if (!$this->_projectExist($project_id)) {
            $this->output->send(['errors' => 'error.project_not_exist']);
        }

        $data = json();

        $validation = $this->_setValidation($data);

        if (!$validation->run($this)) {
            $this->output->send($validation->get_errors(), 422);
        }

        $project = Project::where('id', $project_id)->first();
        $project->fillable([
            'name',
            'type',
            'description',
            'date_start',
            'date_end',
            'time_limit',
            'payment'
        ]);
        $project->fill($data);

        if (!$project->save()) {
            $this->output->send(['project' => 'error.edit_project'], 500);
            return false;
        }

        $data['file_ids'] = json('file_ids');
        $data['user_ids'] = json('user_ids');

        $this->load->module('api/file/File_Entity_api', null, 'file_entity');
        $this->load->module('api/user/User_Entity_api', null, 'user_entity');

        $this->file_entity->_unbind(Project::ENTITY_PROJECT, $project->id);
        $this->user_entity->_unbind(Project::ENTITY_PROJECT, $project['project_id']);

        //привязываем загруженные файлы к проекту
        if ($data['file_ids']) {
            $this->file_entity->_bind(Project::ENTITY_PROJECT, $project->id, $data['file_ids']);
        }

        //привязываем пользователей к проекту
        if ($data['user_ids']) {
            $this->user_entity->_bind(Project::ENTITY_PROJECT, $project->id, $data['user_ids']);
        }

        $this->output->send($project, 200);

    }

    /*
    |--------------------------------------------------------------------------
    | Удаление проекта
    |--------------------------------------------------------------------------
    */

    function remove($project_id)
    {

        DB::beginTransaction();
        $project = Project::find($project_id);

        if (!$project) {
            $this->output->send(['errors' => 'error.project_not_exist']);
        }

        $file_entities = File_Entity::where([
            'entity_type' => Project::ENTITY_PROJECT,
            'entity_id' => $project_id
        ]);

        $user_entities = User_Entity::where([
            'entity_type' => Project::ENTITY_PROJECT,
            'entity_id' => $project_id
        ]);

        if ($project->delete() && $file_entities->delete() &&$user_entities->delete()) {
            DB::commit();
            $this->output->send(['project_id' => $project->id], 200);
        } else {
            DB::rollback();
            $this->output->send(['errors' => ['delete_project' => 'errors.project.delete']], 500);
        }

    }

    function _projectExist($id)
    {
        return (bool)Project::where('id', $id)->count();
    }

    function tasks()
    {
        $this->load->module('api/task/task_api');

        $list = $this->task_api->_getList(['project_id' => 1]);

        echo '<pre>';
        print_r($list);
        echo '</pre>';

        /*
        $project = $project->with('tasks')->whereHas('tasks', function($query){
            $query->where('id', 2);
        })->get();

        $task = new Task();
        $task = $task->with('project')->whereHas('project', function($query){
            $query->where('id', 1);
        })->get();
        */

        //echo '<pre>'; print_r($project->tasks->toArray()); echo '</pre>';
        //echo '<pre>'; print_r($task->toArray()); echo '</pre>';
        /*
        get 'projects';
        get 'projects/{id}';
        post 'projects/create';
        post 'projects/{id}/update';
        post 'projects/{id}/delete';
        get 'projects/{id}/tasks';
        get 'projects/{id}/tasks/{id}';
        post 'projects/{id}/tasks/create';
        post 'projects/{id}/tasks/{id}/update';
        post 'projects/{id}/tasks/{id}/delete';
        */
    }


}
