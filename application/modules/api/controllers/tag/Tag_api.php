<?php

class User_api extends Api_Controller {

    function __construct(){
        parent::__construct();
    }

    function index(){
        $this->getList();
    }

    function getList(){

    }

    function _getList($filters = null, $sort_by = null, $sort_order = null, $offset = null, $limit = 10){

    }

    function get($id){

    }


    function add(){

    }

    function edit($tag_id){

    }

    function remove($tag_id){

    }


    function _setValidation($data){

        $validation = $this->form_validation;
        $validation->set_data($data);

        $validation->set_rules('user[email]', 'email', 'trim|required|valid_email');

        return $validation;
    }

    function _exist($id){
        return (bool) Tags::where('id', $id)->count();
    }



}