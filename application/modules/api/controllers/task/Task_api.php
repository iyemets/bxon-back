<?php
class Task_api extends Api_Controller{

    function __construct(){
        parent:: __construct();
        $this->load->module('test');
        echo '<pre>'; print_r($this->test->auth); echo '</pre>';
    }

    function index(){
        $this->_getList();
    }

    function _getList($filters = []){
        $task = new Task();

        if(isset($filters['project_id'])){
            $task = $task->whereHas('project', function($query) use ($filters){
                $query->where('id', $filters['project_id']);
            });
        }

        $task = $task->get()->toArray();
        echo '<pre>'; print_r($task); echo '</pre>';
    }

}