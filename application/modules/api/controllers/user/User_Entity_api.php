<?php
class User_Entity_Api extends Api_Controller{
    function __construct(){
        parent:: __construct();
    }

    function bind(){
        $data = json();

        if(empty($data)){
            $this->output->send(422, 'Empty request data');
        }

        //echo '<pre>'; print_r($data); echo '</pre>';

        $validation = $this->form_validation;
        $validation->set_data($data);

        $validation->set_rules('entity_type', 'entity_type', 'required|in_list['.implode(',', User_Entity::ENTITY_TYPES).']');
        
        if(isset($data['entity_id']) && is_array($data['entity_id'])){
            foreach($data['entity_id'] as $key => $entity_id){
                $validation->set_rules('entity_id['.$key.']', 'entity_id', 'required');
            }
        }else{
            $validation->set_rules('entity_id', 'entity_id', 'required');
        }

        if(isset($data['user_id']) && is_array($data['user_id'])){
            foreach($data['user_id'] as $key => $user_id){
                $validation->set_rules('user_id['.$key.']', 'user_id', 'required');
            }
        }else{
            $validation->set_rules('user_id', 'user_id', 'required');
        }

        if( ! $validation->run($this)){
            $this->output->send($validation->get_errors(), 422);
        }

        $result = $this->_bind($data['entity_type'], $data['entity_id'], $data['user_id']);

        if($result){
            $this->output->send($result, 200);
        }else{
            $this->output->send('Eternal server error', 500);
        }
    }

    function _bind($entity_type, $entity_id, $user_id){

        $data = [];
        $entity_ids = [];
        $user_ids = [];

        if(is_array($entity_id)){
            $entity_ids = $entity_id;
        }elseif(is_integer($entity_id)){
            $entity_ids = [$entity_id];
        }

        if(is_array($user_id)){
            $user_ids = $user_id;
        }elseif(is_integer($user_id)){
            $user_ids = [$user_id];
        }

        foreach ($entity_ids as $entity_id) {
            foreach ($user_ids as $user_id) {
                $data[] = [
                    'user_id' => $user_id,
                    'entity_id' => $entity_id,
                    'entity_type' => $entity_type
                ];
            }
        }

        if( ! User_Entity::insert($data)){
            return false;
        }

        return [
            'user_ids' => $user_ids,
            'entity_ids' => $entity_ids
        ];
    }

    function unbind(){
        $this->_unbind(null, null, 1);
    }

    function _unbind($entity_type, $entity_id = null, $user_id = null){
        $user_entity = new User_Entity();

        if($entity_id){
            $user_entity = $user_entity->where([
                'entity_type' => $entity_type,
                'entity_id' => $entity_id
            ]);
        }

        if($user_id){
            $user_entity = $user_entity->where([
                'user_id' => $user_id
            ]);
        }

        if( ! $user_entity->delete()){
            return false;
        }

        return [
            'user_id' => $user_id,
            'entity_id' => $entity_id
        ];
    }
}