<?php
class User_Portfolio_api extends Api_Controller{

    function __construct(){
        parent:: __construct();
    }


    function get($user_id, $resume_id){

    }

    function _getById($user_id = null, $resume_id = null){

        $user = new User_Portfolio();

        if($user_id){
            $user = $user->where('user_id', $user_id);
        }

        if($user_id){
            $user = $user->where('id', $user_id);
        }

        return $user->count() ? $user->first()->toArray() : null;
    }

    function _getList($user_id){
        return User_Portfolio::where('id', $user_id)->get()->toArray();
    }

    function add(){

    }

    function _add($user_id, array $data){
        $now = DateTime::createFromFormat('U', time());
        $data = isset($data[0]) ? $data : [$data];

        $resumes = [];

        foreach($data as $item){
            $resumes[] = [
                'user_id' => $user_id,
                'description' => $item['description'],
                'created_at' => $now->format('Y-m-d H:i:s'),
                'updated_at' => $now->format('Y-m-d H:i:s')
            ];
        }
        return User_Portfolio::insert($resumes);
    }

    function edit(){

    }

    function _edit($user_id, $data){
        $now = DateTime::createFromFormat('U', time());

        $resume = [
            'user_id' => $user_id,
            'description' => $data['description'],
            'updated_at' => $now->format('Y-m-d H:i:s')
        ];

        return User_Portfolio::where('id', $user_id)->update($resume);
    }

    function remove(){

    }

    function _remove($user_id){
        return User_Portfolio::where('id', $user_id)->delete();
    }

    function _exist($resume_id){
        return (bool) User_Portfolio::where('id', $resume_id)->count();
    }

}