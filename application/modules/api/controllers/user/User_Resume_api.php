<?php
class User_Resume_api extends Api_Controller{

    function __construct(){
        parent:: __construct();
    }


    function get($user_id, $resume_id){

    }

    function _getById($user_id = null, $resume_id = null){

        $user = new User_Resume();


        if($user_id){
            $user = $user->where('user_id', $user_id);
        }

        if($user_id){
            $user = $user->where('id', $user_id);
        }

        return $user->count() ? $user->first()->toArray() : null;
    }

    function _getList($user_id){
        return User_resume::where('id', $user_id)->get()->toArray();
    }

    function add(){

    }

    function _add($user_id, $data){
        $resume = new User_Resume();

        $data['user_id'] = $user_id;
        $resume->fill($data);

        if( ! $resume->save()){
            return false;
        }

        if(isset($data['sites'])){
            $this->_addSites($resume->id, $data['sites']);
        }

        return $resume->id;
    }

    function edit(){

    }

    function _edit($user_id, $data){
        $resume = new User_Resume();
        $resume = $resume->where([
            'user_id' => $user_id
        ])->first();

        if( ! $resume->count()) return false;

        $resume->fill($data);

        if( ! $resume->save()){
            return false;
        }

        if(isset($data['sites'])){
            $this->_removeSites($resume->id);
            $this->_addSites($resume->id, $data['sites']);
        }

        return $resume->id;
    }

    function remove(){

    }

    function _remove($user_id){
        return User_Resume::where('id', $user_id)->delete();
    }

    function _removeSites($resume_id){
        return site::where([
            'entity_type' => Site::ENTITY_TYPE_RESUME,
            'entity_id' => $resume_id
        ])->delete();
    }

    function _addSites($resume_id, $data){
        $sites = [];

        if(is_array($data)){
            $sites = $data;
        }elseif(is_string($data)){
            $sites = [$data];
        }

        foreach($sites as $key => $site){
            $sites[$key] = [
                'entity_type' => Site::ENTITY_TYPE_RESUME,
                'entity_id' => $resume_id,
                'url' => $site
            ];
        }

        return Site::insert($sites);
    }

    function _exist($resume_id){
        return (bool) User_Resume::where('id', $resume_id)->count();
    }

}