<?php
class User_Shedule_api extends Api_Controller{

    function __construct(){
        parent:: __construct();
    }

    function add(){

    }

    function _add($user_id, $data){
        foreach($data as $key => $value){
            $data[$key]['user_id'] = $user_id;
            $data[$key]['week_day'] = $key;
        }
        return User_Shedule::insert($data);
    }

    function remove(){

    }

    function _remove($user_id, $week_day = null){
        $user_shedule = new User_Shedule();

        $user_shedule = $user_shedule->where('user_id', $user_id);

        if($week_day){
            $user_shedule = $user_shedule->where('week_day', $week_day);
        }

        return $user_shedule->delete();
    }

}