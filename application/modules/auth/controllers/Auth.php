<?php

use \Firebase\JWT\JWT;

class Auth extends MX_Controller{

    function __construct(){
        parent::__construct();
        $this->load->model('user');
        $this->load->config('auth');
    }

    function index(){
        echo 'auth/index';
    }

    function login(){
        $email = $this->input->json('email');
        $password = $this->input->json('password');

        if($email && $password){
            $user = User::where([
                'email' => $email,
                'password' => md5($password)
            ]);

            if( ! $user->count()){
                $this->output->send(['error' => 'Authorization failed'], 401, 'json');
            }

            $user = $user->first();

            $token = $this->_generateToken($user->id);

            $this->output->send(['token' => $token], 200, 'json');
        }

        $this->output->send(['error' => 'Authorization failed'], 401, 'json');
    }

    function _generateToken($user_id){
        $sessionId = uniqid();
        $key = 'secret';
        $data = array(
            'jti' => $sessionId,
            'iat' => time(),
            'nbf' => time(),
            'exp' => time() + $this->config->item('expiration'),
            'data' => [
                'user_id' => $user_id
            ]
        );

        return JWT::encode($data, $key);
    }

    function _checkToken(){
        $token = $this->input->get_request_header('Authorization');
        $key = 'secret';

        try{
            $data = JWT::decode(str_replace('Bearer ', '', $token), $key, array('HS256'));
        }catch (Exception $e){
            return false;
        }
        return $data->data;
    }
}