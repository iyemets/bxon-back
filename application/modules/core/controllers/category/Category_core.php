<?php
class Category_core extends MY_Controller{

    function __construct(){
        parent::__construct();

    }

    function index(){
        return $this->_getList();
    }

    /*
    |--------------------------------------------------------------------------
    | Получаем список категорий
    |--------------------------------------------------------------------------
    */

    function _getList($filters = null, $sort_by = null, $sort_order = null, $limit = null, $offset = null){

        if( ! in_array($sort_by, ['parent_id', 'entity_type'])){
            $sort_by = 'id';
        }

        if( ! in_array(mb_strtolower($sort_order), ['asc', 'desc'])){
            $sort_by = 'asc';
        }

        $limit = (int) $limit;

        if($limit < 1 && $limit > 100){
            $limit = 10;
        }

        $offset = (int) $offset;


        $categories = new Category();

        if(isset($filters['id'])){

        }

        return $categories->toArray();
    }

    /*
    |--------------------------------------------------------------------------
    | Получаем категорию по ID
    |--------------------------------------------------------------------------
    */

    function _getById($id){
        $category = Category::find($id);

        if( ! $category){
            return null;
        }

        return $category->toArray();
    }


    /*
    |--------------------------------------------------------------------------
    | Привязываем категории к сущности
    |--------------------------------------------------------------------------
    */

    function _bind($entity_type, $entity_id, $ids = []){

        $category_ids = Category::where('entity_type', $entity_type)
            ->whereIn('id', $ids)->pluck('id')->toArray();

        if (empty($category_ids)) return false;

        $category_entity_ids = Category_Entity::where([
            'entity_id' => $entity_id,
            'entity_type' => $entity_type
        ])->whereIn('category_id', $ids)->pluck('category_id')->toArray();

        foreach ($ids as $id) {

            if (!in_array($id, $category_ids) || in_array($id, $category_entity_ids)) continue;

            $insert[] = [
                'entity_id' => $entity_id,
                'entity_type' => $entity_type,
                'category_id' => $id
            ];
        }

        if (!empty($insert)){
            Category_Entity::insert($insert);
            return true;
        }

        return false;
    }

    /*
    |--------------------------------------------------------------------------
    | Отвязываем сущность от категории
    |--------------------------------------------------------------------------
    */

    function _unbind($entity_type, $entity_id)
    {
        $remove = Category_Entity::where([
            'entity_id' => $entity_id,
            'entity_type' => $entity_type
        ])->delete();

        if ($remove) return true;

        return false;
    }

    /*
    |--------------------------------------------------------------------------
    | Получаем категории определенной сущности
    |--------------------------------------------------------------------------
    */

    function _getCategoryEntity($entity_type, $entity_ids)
    {
        if (!is_array($entity_ids)) $entity_ids = array($entity_ids);

        $category_entities = Category_Entity::where('entity_type', $entity_type)
            ->whereIn('entity_id', $entity_ids)->with('category')->get();


        if (empty($category_entities)) return false;

        $entity = [];
        foreach ($category_entities as $category){
            $category_ids[] = $category['category_id'];
            $entity[$category['entity_id']][] = [
                'id' => $category['category'][0]['id'],
                'name' => $category['category'][0]['name']
            ];
        }

        if (!empty($entity)){
            return $entity;
        }

        return [];
    }
 }