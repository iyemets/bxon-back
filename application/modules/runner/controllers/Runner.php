<?php
class Runner extends MX_Controller{

    protected $response_type = 'json';

    function __construct(){}

    function index(){
        $segments = $this->uri->segment_array();

        $module = array_shift($segments);
        $method = array_shift($segments);

        $args = [];
        $args[] = 'api/' . $module . '/' . $module . '_api/' . $method;
        $args[] = $module . '_api';

        foreach($segments as $segment){
            $args[] = $segment;
        }

        $result = call_user_func_array(['Modules', 'run'], $args);

        if( ! $result){
            $this->output->send('page not found', 404);
        }

        echo $result;
    }

    function rest(){
        $segments = $this->uri->segment_array();

        $parent = isset($segments[1]) ? $segments[1] : 'index';
        $method = isset($segments[2]) ? $segments[2] : 'index';
        $module_name = $parent . '_api';
        $path = $module_name;

        if($this->_moduleExist($path) && is_callable([$module_name, $method])){
            $this->_callModule($path, $method, array_slice($segments, 2));
        }

        $parent = isset($segments[1]) ? $segments[1] : 'index';
        $child = isset($segments[2]) ? $segments[2] : 'index';
        $method = isset($segments[3]) ? $segments[3] : 'index';
        $module_name = ucfirst($parent) . '_' . ucfirst($child) . '_api';
        $path = $module_name;

        if($this->_moduleExist($path) && is_callable([$module_name, $method])){
            $this->_callModule($path, $method, array_slice($segments, 3));
        }

        $parent = isset($segments[1]) ? $segments[1] : 'index';
        $method = isset($segments[2]) ? $segments[2] : 'index';
        $module_name = ucfirst($parent) . '_api';
        $path = $parent . '/' . $module_name;

        if($this->_moduleExist($path) && is_callable([$module_name, $method])){
            $this->_callModule($path, $method, array_slice($segments, 2));
        }

        $parent = isset($segments[1]) ? $segments[1] : 'index';
        $child = isset($segments[2]) ? $segments[2] : 'index';
        $method = isset($segments[3]) ? $segments[3] : 'index';
        $module_name = ucfirst($parent) . '_' . ucfirst($child) . '_api';
        $path = $parent . '/' . $module_name;

        if($this->_moduleExist($path) && is_callable([$module_name, $method])){
            $this->_callModule($path, $method, array_slice($segments, 3));
        }

        $this->output->send('not found');
    }

    function _moduleExist($module){

        $ds = DIRECTORY_SEPARATOR;

        $module = str_replace('/', $ds, $module);

        $module = explode($ds, $module);

        $module_name = array_pop($module);
        $module_path = implode($ds, $module) . $ds;

        $file_path = str_replace($ds . $ds, $ds,  APPPATH . 'modules' . $ds . 'api' . $ds . 'controllers' . $ds . $module_path . $module_name . '.php');

        if( ! file_exists($file_path) || ! is_readable($file_path))
            return false;

        require_once $file_path;

        return class_exists($module_name);
    }

    function _callModule($module, $method, $arguments = []){
        $this->load->module('api/' . $module, null, 'callable_module');
        call_user_func_array([$this->callable_module, $method], $arguments);exit();
    }



}