<?php
	$stats = json_decode(file_get_contents('stats.json'));
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to CodeIgniter</title>

	<?php
	foreach ($stats->assets as $asset) {
		$file_parts = pathinfo($asset->name);

		if ($file_parts['extension'] == 'css') {
			echo '<link href="' . $stats->publicPath . $asset->name  .'" rel="stylesheet">';
		}
	}
	?>
</head>
<body>
	<div id="root"></div>

	<?php
	function getJsAsset($name, $stats) {
		foreach ($stats->assets as $asset) {
			$file_parts = pathinfo($asset->name);

			if ($file_parts['extension'] == 'js' && strrpos($asset->name, $name) !== false) {
				echo '<script type="text/javascript" src="' . $stats->publicPath . $asset->name  .'"></script>';
			}
		}
	}

	getJsAsset('runtime', $stats);
	getJsAsset('vendor', $stats);
	getJsAsset('main', $stats);
	?>
</body>
</html>
